import controller.requests_scripts.corona_req as cr
from apscheduler.schedulers.blocking import BlockingScheduler

sched = BlockingScheduler()

@sched.scheduled_job('interval',minutes=120)
def timed_job():
    print("Executando job")
    cr.iniciar()
    print("job finalizado")


sched.start()
#timed_job()
