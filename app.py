# links abaixo para fazer cronometro e executar sozinho
# https://saqibameen.com/deploy-python-cron-job-scripts-on-heroku/
# http://www.modeo.co/blog/2015/1/8/heroku-scheduler-with-nodejs-tutorial

import flask
import requests
import json
import controller.get_data as gd
from flask import request, jsonify, render_template, redirect, url_for
from flask_cors import CORS

app = flask.Flask(__name__)
CORS(app)

"""
Routers
"""
@app.route('/',methods=['GET'])
def home():
    return render_template('home.html')

@app.route('/coronavirus/all',methods=['GET'])
def coronavirus_all():
    return jsonify(gd.get_covid_generaldata())

@app.route('/coronavirus/all_lower',methods=['GET'])
def coronavirus_all_lower():
    return jsonify(gd.get_covid_generaldata_lower())

@app.route('/bcg/all', methods=['GET'])
def bcg_all():
    return jsonify(gd.get_bcg_generaldata())

@app.route('/coronavirus/indice_infectados_mortos',methods=['GET'])
def indice_mortos():
    return jsonify(gd.indice_infectados_mortos())

@app.route('/coronavirus/indice_infectados_mortos_lower',methods=['GET'])
def indice_mortos_lower():
    return jsonify(gd.indice_infectados_mortos_lower())

@app.route('/paises/all', methods=['GET'])
def paises():
    return jsonify(gd.get_sigla_pais())

@app.route('/paises/all_lower',methods=['GET'])
def paises_lower():
    return jsonify(gd.get_sigla_pais_lower())

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True)
