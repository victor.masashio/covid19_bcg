import requests
import json
from datetime import datetime
from bs4 import BeautifulSoup

def verificarStatus(msg):
    if 'Current national BCG vaccination policy for all' in msg:
        return 'POLITICA_APLICADA'

    elif 'BCG recommendation only for specific groups' in msg:
        return 'POLITICA_RECOMENDADA'

    elif 'Past national BCG vaccination policy for all' in msg:
        return 'POLITICA_ALTERADA'

    else:
        pass

def verificarAdicionais(html):
    soup = BeautifulSoup(html, 'html.parser')
    tr = soup.select('tr')
    ano_introducao = None
    porcentagem = None

    for td in tr:
        if 'vaccination introduced' in td.contents[0].text:
            if('Unknow' not in td.contents[2].text):
                ano_introducao = td.contents[2].text

        if 'BCG coverage (%)' in td.contents[0].text:
            porcentagem = td.contents[2].text

    return {'ano_introducao': ano_introducao, 'porcentagem_cobertura': porcentagem}


def get_bcg():
    resultList = requests.get('http://bcgatlas.org/data.php').json()
    request_date = datetime.now()
    json_result = {}

    for linha in resultList:
        if linha['groupID'] != "" and linha['groupID'] != None and linha['id'] != None:
            sigla = linha['id']
            status = verificarStatus(linha['groupID'])
            adicionais = verificarAdicionais(linha['info'])

            json_result[sigla] = {
                'status': status,
                'adicionais': adicionais
            }

    with open('../json/bcg.json','w') as outfile:
        json.dump(json_result, outfile, indent=3)

