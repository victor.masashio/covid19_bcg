import requests
import json
from datetime import datetime


def get_covid19():
    url = "https://dashboards-dev.sprinklr.com/data/9043/global-covid19-who-gis.json"
    resultList = requests.request("GET", url).json()['rows']
    json_result = {}
    pais = ""
    confirmados = 0
    mortes = 0
    total = 0

    for linha in resultList:
        pais = linha[1]
        ultimas_mortes = linha[3]
        mortes_total = linha[4]
        ultimos_confimados = linha[5]
        confirmados_total = linha[6]

        json_result[pais] = {
            'Confirmados': confirmados_total,
            'Mortes': mortes_total,
            'Novos casos': {
                'Confirmados': ultimos_confimados,
                'Mortes': ultimas_mortes
            }
        }
        total += confirmados
    with open('../json/coronavirus.json','w') as outfile:
        json.dump(json_result, outfile, indent=3)

