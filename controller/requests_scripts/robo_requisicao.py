import datetime
import time
import corona_req as cr
import bcg_req as br

def ativarRobo():
    cr.iniciar()
    pass

if __name__ == '__main__':
    print('=--=' * 15)
    tempo_espera = 7200

    while True:
        ativarRobo()

        print(f'Requisição feita as {datetime.datetime.now().strftime("%H:%M:%S - %d/%m" )}')
        print(
            'A Proxima requisição será feita as ' +
            (datetime.datetime.now() + datetime.timedelta(seconds=tempo_espera)).strftime("%H:%M:%S"))
        print('=--=' * 15)

        cont = tempo_espera

        for i in range(0, tempo_espera):
            cont -= 1
            m, s = divmod(cont, 60)
            h, m = divmod(m, 60)

            print(f"\rProxima requisição: {h}h {m}min {s}seg", end="")
            time.sleep(1)

        print('\r', end="")

