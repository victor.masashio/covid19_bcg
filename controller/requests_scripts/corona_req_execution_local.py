import requests
import json
from datetime import datetime


def iniciar():
    url = "https://dashboards-dev.sprinklr.com/data/9043/global-covid19-who-gis.json"
    resultList = requests.request("GET", url).json()['rows']
    request_date = datetime.now()
    json_result = {'data_requisicao':str(request_date)}
    pais = ""
    confirmados = 0
    mortes = 0
    total = 0

    for linha in resultList:
        pais = linha[1]
        ultimas_mortes = linha[3]
        mortes_total = linha[4]
        ultimos_confimados = linha[5]
        confirmados_total = linha[6]

        json_result[pais] = {
            'Confirmados': confirmados_total,
            'Mortes': mortes_total,
            'Novos_casos': {
                'Confirmados': ultimos_confimados,
                'Mortes': ultimas_mortes
            }
        }
        total += confirmados
    with open('../../json/coronavirus.json','w') as outfile:
        json.dump(json_result, outfile, indent=3)

    """
    Codigo abaixo apenas para retornar com as ID's minusculas
    para a plataforma
    """

    json_result_idlower = {'data_requisicao':str(request_date)}

    for linha in resultList:
        pais = str(linha[1])
        ultimos_mortes = linha[3]
        mortes_total = linha[4]
        ultimos_confirmados = linha[5]
        confirmados_total = linha[6]

        json_result_idlower[pais.lower()] = {
            'Confirmados': confirmados_total,
            'Mortes': mortes_total,
            'Novos casos': {
                'Confirmados': ultimos_confimados,
                'Mortes': ultimas_mortes
            }
        }
        total += confirmados
    with open('../../json/coronavirus_lower.json','w') as outfile:
        json.dump(json_result_idlower, outfile, indent=3)


iniciar()
