import json

def indice_infectados_mortos():
    lines=""
    indice = {}
    with open('json/coronavirus.json','r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            lines += line

    dict_total = json.loads(lines)
    for pais in dict_total:
        if pais == "data_requisicao":
            continue
        total_mortos_infectados = (dict_total[pais]["Mortes"] / dict_total[pais]["Confirmados"]) * 100
        indice[f"{pais}"] = f"{total_mortos_infectados:.2f}"
    return indice

def indice_infectados_mortos_lower():
    lines=""
    indice = {}
    with open('json/coronavirus_lower.json','r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            lines += line

    dict_total = json.loads(lines)
    for pais in dict_total:
        if pais == "data_requisicao":
            continue
        total_mortos_infectados = (dict_total[pais]["Mortes"] / dict_total[pais]["Confirmados"]) * 100
        indice[f"{pais}"] = f"{total_mortos_infectados:.2f}"
    return indice


def get_covid_generaldata():
    lines = ""
    with open('json/coronavirus.json','r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            lines += line
    return json.loads(lines)

def get_covid_generaldata_lower():
    lines = ""
    with open('json/coronavirus_lower.json','r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            lines += line
    return json.loads(lines)


def get_bcg_generaldata():
    lines = ""
    with open('json/bcg.json','r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            lines += line
    return json.loads(lines)

def get_sigla_pais():
    lines = ""
    with open('json/sigla_pais.json','r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            lines += line
        return json.loads(lines)

def get_sigla_pais_lower():
    lines = ""
    with open('json/sigla_pais_lower.json','r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            lines += line
        return json.loads(lines)


def get_europe_covid():
    europe = [
        'Albania',
        'Andorra',
        'Armenia',
        'Austria',
        'Azerbarijan',
        'Belarus',
        'Belgium',
        'Bosnia and Herzegovina',
        'Bulgaria',
        'Croatia',
        'Cyprus',
        'Czechia',
        'Denmark',
        'Estonia',
        'Finland',
        'France',
        'Georgia',
        'Germany',
        'Greece',
        'Hungary',
        'Iceland',
        'Ireland',
        'Italy',
        'Kazakhstan',
        'Kosovo',
        'Latvia',
        'Liechtenstein',
        'Lithuania',
        'Luxembourg',
        'Malta',
        'Moldova',
        'Monaco',
        'Montenegro',
        'Netherlands',
        'Macedonia',
        'Norway',
        'Poland',
        'Portugal',
        'Romania',
        'Russia',
        'San Marino',
        'Serbia',
        'Slovakia',
        'Slovenia',
        'Spain',
        'Sweden',
        'Switzerland',
        'Turkey',
        'Ukraine',
        'United Kingdom',
        'Vatican City'
            ]
