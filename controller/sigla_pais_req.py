import requests
import json
from bs4 import BeautifulSoup

def get_sigla_pais():
    url = "https://www.realifewebdesigns.com/web-marketing/abbreviations-countries.asp"
    r = requests.get(url)
    data = r.text
    soup = BeautifulSoup(data, 'html.parser')

    t1 = [tag.text.replace("<l1>","") for tag in soup.find_all("li")]
    t1.sort()

    #sigla-pais
    sp = []
    for element in t1:
        if "=" in element:
            sp.append(element)

    sigla_pais = {}
    for element in sp:
        temp = element.split(" = ")
        sigla_pais[temp[0]] = temp[1]


    with open('../json/sigla_pais.json','w') as outfile:
        json.dump(sigla_pais, outfile, indent=3)

get_sigla_pais()
